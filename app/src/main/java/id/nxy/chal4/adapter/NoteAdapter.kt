package id.nxy.chal4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.nxy.chal4.R
import id.nxy.myapp.database.Note


class NoteAdapter(private val onDelete: (Note) -> Unit,
                  private val onEdit: (Note) -> Unit,
                  private val listener: NoteActionListener)
    : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    private val difCallback = object : DiffUtil.ItemCallback<Note>() {

        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }

    private val differ = AsyncListDiffer(this, difCallback)

    fun updateData(note: List<Note>) = differ.submitList(note)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return NoteViewHolder(view)
    }


    override fun getItemCount(): Int = differ.currentList.size

    inner class NoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvId = view.findViewById<TextView>(R.id.tv_id_value)
        val tvJudul = view.findViewById<TextView>(R.id.tv_catatan_title)
        val tvCatatan = view.findViewById<TextView>(R.id.tv_catatan_value)
        val btnDelete = view.findViewById<ImageView>(R.id.btn_delete)
        val btnEdit = view.findViewById<ImageView>(R.id.btn_edit)

        fun bind(note: Note) {
            tvId.text = note.id.toString()
            tvJudul.text = note.judul
            tvCatatan.text = note.catatan

            btnDelete.setOnClickListener {
                onDelete.invoke(note)
                listener.onDelete(note)
            }

            btnEdit.setOnClickListener {
                onEdit.invoke(note)
                listener.onEdit(note)
            }
        }

    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

}
interface NoteActionListener {
    fun onDelete(note: Note)
    fun onEdit(note: Note)
}

