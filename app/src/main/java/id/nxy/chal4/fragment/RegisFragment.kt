package id.nxy.chal4.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import id.nxy.chal4.databinding.FragmentRegisBinding
import id.nxy.myapp.database.User
import id.nxy.myapp.database.UserDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegisFragment : Fragment() {
    private var _binding: FragmentRegisBinding? = null
    private val binding get() = _binding!!

    private var db: UserDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = UserDatabase.getInstance(requireContext().applicationContext)
        checkConfirmPasswordOnChange()
        binding.btnRegis.setOnClickListener{
            insertData(it)
        }
    }

    private fun insertData(view : View) {
        val users = User(
            username = binding.etUsername.text.toString(),
            email= binding.etEmail.text.toString(),
            password = binding.etPassword.text.toString()
        )
        CoroutineScope(Dispatchers.IO).launch {
            val result = db?.userDao()?.insert(users)
            requireActivity().runOnUiThread {
                if (result != 0L) {
                    Toast.makeText(requireContext(), "Register Berhasil", Toast.LENGTH_SHORT)
                        .show()
                    view.findNavController().navigateUp()
                } else {
                    Toast.makeText(requireContext(), "Register Gagal", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun checkConfirmPasswordOnChange() {
        binding.etConfPass.addTextChangedListener(textWatcher)
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(p0: Editable?) {
            if (checkConfirmPassword() || p0.isNullOrEmpty()) {
                binding.etConfPass.error = null
            } else {
                binding.etConfPass.error = "tidak sesuai"
            }
        }
    }

    private fun checkConfirmPassword(): Boolean {
        binding.apply {
            return etPassword.text.toString() == etConfPass.text.toString()
        }
    }

}