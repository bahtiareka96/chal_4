package id.nxy.chal4.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.edit
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.nxy.chal4.R
import id.nxy.chal4.adapter.NoteActionListener
import id.nxy.chal4.adapter.NoteAdapter
import id.nxy.chal4.databinding.FragmentHomePageBinding
import id.nxy.myapp.database.Note
import id.nxy.myapp.database.UserDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomePageFragment : Fragment() {
    private var _binding: FragmentHomePageBinding? = null
    private val binding get() = _binding!!

    private var db: UserDatabase? = null
    private lateinit var noteAdapter: NoteAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomePageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = UserDatabase.getInstance(requireContext().applicationContext)
        initRecyclerView()
        getDataFromDb()
        addNote()
        val pref = requireActivity().getSharedPreferences(
            LoginFragment.PREF_NAME,
            Context.MODE_PRIVATE
        )
        btnLogOutClicked(pref)

    }

    private fun initRecyclerView() {
        binding.apply {
            noteAdapter = NoteAdapter({}, {}, action)
            rvData.adapter = noteAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun addNote() {
        binding.fabAdd.setOnClickListener {
            showAlertDialog(null, true)
        }
    }

    private fun showAlertDialog(note: Note?, isFromButtonAdd: Boolean) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_add, null, false)

        val etJudul = customLayout.findViewById<EditText>(R.id.et_judul)
        val etCatatan = customLayout.findViewById<EditText>(R.id.et_catatan)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        if (!isFromButtonAdd && note != null) {
            etJudul.setText(note.judul)
            etCatatan.setText(note.catatan)
            btnSave.text = "Simpan"
        }

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        btnSave.setOnClickListener {
            val judul = etJudul.text.toString()
            val catatan = etCatatan.text.toString()
            if (note != null) {
                val newNote = Note(note.id, userId = null, judul, catatan)
                updateToDb(newNote)
            } else {
                saveToDb(judul, catatan)
                checkRegisteredNote(judul, catatan)
            }
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun saveToDb(judul: String, catatan: String) {
        val note = Note(null, null ,judul, catatan)
        CoroutineScope(Dispatchers.IO).launch {
            val result = db?.noteDao()?.insertNote(note)
            if (result != 0L) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil Ditambahkan", Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal Ditambahkan", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun checkRegisteredNote(judul: String, catatan: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = db?.noteDao()?.getRegisteredNote(judul, catatan)
            if (!result.isNullOrEmpty()) {
                result[0].id
                result[0].judul
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Note telah ada", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Note belum ada", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getDataFromDb() {
        CoroutineScope(Dispatchers.IO).launch {
            val result = db?.noteDao()?.getAllNote()
            if (result != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    noteAdapter.updateData(result)
                }
            }
        }
    }

    private fun updateToDb(note: Note) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = db?.noteDao()?.updateNote(note)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil Diupdate", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal Diupdate", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private val action = object : NoteActionListener {
        override fun onDelete(note:Note) {
            CoroutineScope(Dispatchers.IO).launch {
                val result = db?.noteDao()?.deleteNote(note)
                if (result != 0) {
                    getDataFromDb()
                    CoroutineScope(Dispatchers.Main).launch {
                        Toast.makeText(requireContext(), "Berhasil Dihapus", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    CoroutineScope(Dispatchers.Main).launch {
                        Toast.makeText(requireContext(), "Gagal Dihapus", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        override fun onEdit(note: Note) {
            showAlertDialog(note, false)
        }

    }

    private fun btnLogOutClicked(pref: SharedPreferences) {
        binding.logout.setOnClickListener {
            pref.edit{
                clear()
                apply ()
            }
            findNavController().navigate(HomePageFragmentDirections.actionHomePageFragmentToLoginFragment())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}