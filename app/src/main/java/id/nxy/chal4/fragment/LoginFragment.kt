package id.nxy.chal4.fragment

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.nxy.chal4.R
import id.nxy.chal4.databinding.FragmentLoginBinding
import id.nxy.myapp.database.User
import id.nxy.myapp.database.UserDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var db: UserDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRegisClicked()
        db = UserDatabase.getInstance(requireContext().applicationContext)
        val pref = requireActivity().getSharedPreferences(PREF_NAME,MODE_PRIVATE)
        val prefEmail = pref.getString(KEY_EMAIL,"")
        val prefPassword = pref.getString(KEY_PASS,"")
        if(prefEmail!!.isNotEmpty()&& prefPassword!!.isNotEmpty()){
            view.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomePageFragment())
        }
        binding.btnLogin.setOnClickListener {
            loadData(it,pref)
        }

    }

    private fun loadData(view: View, pref: SharedPreferences) {
        val user = User(
            email = binding.etEmail.text.toString(),
            password = binding.etPassword.text.toString()
        )
        GlobalScope.launch{
            val result = db?.userDao()?.getUser(user.email!!,user.password!!)
            requireActivity().runOnUiThread {
                if (result != null){
                    view.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomePageFragment())
                    setPreferences(pref,result)
                } else {
                    Toast.makeText(requireContext(),"Data tidak ada", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setPreferences(pref: SharedPreferences, user: User){
        pref.edit {
            putString(KEY_EMAIL, user.email!!)
            putString(KEY_NAME, user.username!!)
            putString(KEY_PASS, user.password!!)
            apply()
        }
    }

    private fun btnRegisClicked() {
        binding.toRegis.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_regisFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val KEY_EMAIL = "email"
        const val KEY_NAME = "username"
        const val KEY_PASS = "password"
        const val PREF_NAME = "pref_name"
    }



}