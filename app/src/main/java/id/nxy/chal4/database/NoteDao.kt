package id.nxy.myapp.database

import androidx.room.*

@Dao
interface NoteDao {
    @Query("SELECT * FROM Note")
    fun getAllNote() : List<Note>

    @Query("SELECT * FROM Note WHERE judul = :judul AND catatan = :catatan ")
    fun getRegisteredNote(judul: String, catatan: String) : List<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(note:Note) : Long

    @Update
    fun updateNote(note: Note) : Int

    @Delete
    fun deleteNote(note: Note) : Int
}