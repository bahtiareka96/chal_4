package id.nxy.myapp.database

import androidx.room.*


@Dao
interface UserDao {
//    @Query("SELECT * FROM User")
//    fun getAllUser() : List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User) : Long

    @Update
    fun updateUser(user: User) : Int

    @Delete
    fun deleteUser(user: User) : Int

    @Insert
    suspend fun insert(register : User): Long

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    suspend fun getUser(username: String, password : String): User?
}