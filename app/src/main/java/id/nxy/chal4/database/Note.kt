package id.nxy.myapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Note(
    @PrimaryKey(autoGenerate = true)
    var id : Int? = null,
    @ColumnInfo(name = "userId")
    var userId: Int? = null,
    @ColumnInfo(name = "judul")
    var judul: String? = null,
    @ColumnInfo(name = "catatan")
    val catatan: String? = null
)
