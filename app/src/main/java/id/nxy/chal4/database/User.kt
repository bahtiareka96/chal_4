package id.nxy.myapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true)
    var id : Int? = null,
    @ColumnInfo(name = "username")
    var username : String? = null,
    @ColumnInfo(name = "email")
    var email : String? = null,
    @ColumnInfo(name = "password")
    var password : String? = null
)
